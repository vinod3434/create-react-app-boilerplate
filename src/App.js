// @flow
import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

// custom components
import asyncComponent from "./components/AsyncComponent";
import "./App.css";

const AsyncHome = asyncComponent(() => import("./views/Home"));
const AsyncProfile = asyncComponent(() => import("./views/Profile"));
const AsyncIBHubs = asyncComponent(() => import("./views/iBHubs"));
// here would go some application default layout, if it exist
// in our case just simple router
const Root = ({ childProps }) =>
    <Router>
        <Switch>
            <Route exact path="/" component={AsyncHome} props={childProps} />
            <Route
                exact
                path="/profile"
                component={AsyncProfile}
                props={childProps}
            />
            <Route
                exact
                path="/ib/:testing"
                component={AsyncIBHubs}
                props={childProps}
            />
        </Switch>
    </Router>;

export default Root;
