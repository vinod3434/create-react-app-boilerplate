import React, { Component } from "react";
import Helmet from "react-helmet";

class iBHubs extends Component {
    render() {
        console.log(this.props);
        let { match } = this.props;
        let { params } = match;
        let { testing } = params;
        return (
            <div>
                <Helmet title="iB Hubs" />
                <h1>
                    Welcome to iB Hubs {testing}
                </h1>
            </div>
        );
    }
}

export default iBHubs;
