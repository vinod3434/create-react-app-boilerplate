import React, { Component } from "react";
import Helmet from "react-helmet";
import logo from "../../logo.svg";
import { Link } from "react-router-dom";

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "Mani"
        };
    }

    render() {
        let { name } = this.state;
        return (
            <div className="App">
                <Helmet title="Welcom to Home" />
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h2>
                        Welcome to <Link to="/profile">React</Link>
                    </h2>
                </div>
                <p className="App-intro">
                    To get started, edit <code>src/App.js</code> and save to
                    reload.
                </p>
                <p>
                    <Link to="/ib/mani">iB Hubs</Link>
                </p>
            </div>
        );
    }
}

export default Home;
