import React from "react";
// import ReactDOM from "react-dom";
import { render } from "react-snapshot";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
// adding redux
import { Provider } from "react-redux";
import { createStore } from "redux";
import appReducer from "./reducers";

// creating store with our application reducer as state
let store = createStore(appReducer);

// ReactDOM.render(
render(
    // wrapping our App component inside Provider
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById("root")
);
registerServiceWorker();
